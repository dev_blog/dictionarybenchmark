﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace DictionaryBenchmark
{
    public static class BoxingBenchmark
    {
        public static void Comparer<T>( IEqualityComparer<T> comparer, 
                                        Func<int, T> factory, 
                                        int dictionarySize,
                                        int iterations,
                                        StringBuilder sb) where T:struct
        {
            var stopWatch = new Stopwatch();
            var dictionaryDefaultComparer = new Dictionary<T, int>();
            var dictionaryOverridenComparer = new Dictionary<T, int>(comparer);
            
            for (var i = 0; i < dictionarySize; ++i)
            {
                var s = factory(i);
                dictionaryDefaultComparer.Add(s, i);
                dictionaryOverridenComparer.Add(s, i);
            }

            var key = dictionaryDefaultComparer.Keys.ToArray()[dictionaryDefaultComparer.Keys.Count / 2];
            stopWatch.Start();
            for (var i = 0; i < iterations; ++i)
            {
                var item = dictionaryDefaultComparer[key];
            }
            sb.AppendLine($"Dictionary {typeof(T).Name} default comparer {stopWatch.ElapsedMilliseconds} ms");
            
            stopWatch.Restart();
            
            for (var i = 0; i < iterations; ++i)
            {
                var item = dictionaryOverridenComparer[key];
            }
            sb.AppendLine($"Dictionary {typeof(T).Name} overriden comparer {stopWatch.ElapsedMilliseconds} ms");

            stopWatch = null;
        }
        
        public static void IEquitable<T>( int iterations, int dictionarySize, Func<int, T> factory, StringBuilder sb) where T:struct, IEquatable<T>
        {
            var stopWatch = new Stopwatch();
            var dictionary = new Dictionary<T, int>();
            
            for (var i = 0; i < dictionarySize; ++i)
            {
                var s = factory(i);
                dictionary.Add(s, i);
            }
            
            var key = dictionary.Keys.ToArray()[dictionary.Keys.Count / 2];
            stopWatch.Start();
            for (var i = 0; i < iterations; ++i)
            {
                var item = dictionary[key];
            }
            sb.AppendLine($"Dictionary {typeof(T).Name} with implemented IEquitable<T> {stopWatch.ElapsedMilliseconds} ms");

        }

    }
}