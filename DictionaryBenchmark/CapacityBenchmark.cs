﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace DictionaryBenchmark
{
    public static class CapacityBenchmark
    {
        public static void Benchmark(int capacity, StringBuilder sb)
        {
            var dictionary = new Dictionary<object, int>();
            var dictionaryWithCapcity = new Dictionary<object, int>(capacity);

            var keys = new object[capacity];
            for (var i = 0; i < capacity; ++i)
            {
                keys[i] = new object();
            }

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            for (var i = 0; i < capacity; ++i)
            {
                dictionary.Add(keys[i], i);
            }
            sb.AppendLine($"Dictionary population with no capacity specified {stopwatch.ElapsedMilliseconds} ms");
            
            stopwatch.Restart();     
            for (var i = 0; i < capacity; ++i)
            {
                dictionaryWithCapcity.Add(keys[i], i);
            }
            sb.AppendLine($"Dictionary population with capacity specified {stopwatch.ElapsedMilliseconds} ms");
            
        }
    }
}