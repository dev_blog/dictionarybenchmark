﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;

namespace DictionaryBenchmark
{
    public static class DataSetBenchmark
    {
        private const int primeNumer = 36353;
        private static FieldInfo getBuckets = 
            typeof(Dictionary<int, object>).GetField("buckets", BindingFlags.Instance | BindingFlags.NonPublic);

        public static void Benchmark(int iterations, StringBuilder sb)
        {
            var array = GenerateArray(primeNumer);
            var dictionary = GenerateDictionary(array);

            SearchLinear(iterations, array, primeNumer, sb);
            var buckets = (int[]) getBuckets.GetValue(dictionary);
            
            var stopwatch = new Stopwatch();
                   
            stopwatch.Start();
            for (var i = 0; i < iterations; ++i)
            {
                dictionary.ContainsKey( primeNumer );
            }
            
            sb.AppendLine($"Pathological data set lookup time spent {stopwatch.ElapsedMilliseconds} capacity {buckets.Length}" );
            
            dictionary.Add(primeNumer * primeNumer + 1, null);
            buckets = (int[]) getBuckets.GetValue(dictionary);
            
            stopwatch.Restart();
            for (var i = 0; i < iterations; ++i)
            {
                dictionary.ContainsKey( primeNumer );
            }
            
            sb.AppendLine($"Pathological data set lookup time spent {stopwatch.ElapsedMilliseconds} capacity {buckets.Length} " );
        }

        private static void SearchLinear(int iterations, int[] array, int primeNumber, StringBuilder sb)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            for (var i = 0; i < iterations; ++i)
            {
                Array.IndexOf(array, primeNumber * primeNumber);
            }
            sb.AppendLine($"Linear search in array {stopwatch.ElapsedMilliseconds} ms");
        }

        private static Dictionary<int, object> GenerateDictionary(int[] array)
        {
            var dictionary = new Dictionary<int, object>(array.Length );
            foreach (var item in array)
            {
                dictionary[item] = null;
            }

            return dictionary;
        }

        private static int[] GenerateArray(int primeNumber)
        {
            var array = new int[primeNumber];
            for (var i = 0; i < primeNumber; ++i)
            {
                checked
                {
                    array[i] = (i + 1) * primeNumber;
                }          
            }

            return array;
        }
    }
}