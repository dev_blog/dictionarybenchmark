﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DictionaryBenchmark
{
    class Program
    {
        private const int BOXING_ELEMENENTS_SIZE = 1000;
        private const int BOXING_ITERATIONS = 700000;

        private const int DEFAULT_CAPACITY_SIZE = 400000;

        private const int BAD_DATA_DICTIONARY_ITERATIONS = 10000;
        
        static void Main(string[] args)
        {
            var sb = new StringBuilder();
            BoxingBenchmark.Comparer( new CustomValueTypeComparer(), 
                                       (x) => new CustomValueType { StructValue = x }, 
                                       BOXING_ELEMENENTS_SIZE,  BOXING_ITERATIONS, sb);
            
            BoxingBenchmark.IEquitable(BOXING_ITERATIONS, BOXING_ELEMENENTS_SIZE, 
                                        (x) => new CustomValueTypeEquitable { StructValue = x }, sb);
            
            BoxingBenchmark.Comparer( new CustomIntComparer(), 
                                       (x) => x, BOXING_ELEMENENTS_SIZE, BOXING_ITERATIONS, sb);
                       
            
            CapacityBenchmark.Benchmark(DEFAULT_CAPACITY_SIZE, sb);

            DataSetBenchmark.Benchmark(BAD_DATA_DICTIONARY_ITERATIONS, sb);
            
            Console.Write(sb.ToString());
        }
    }

    public struct CustomValueType
    {
        public int StructValue;
    }
    
    public struct CustomValueTypeEquitable : IEquatable<CustomValueTypeEquitable>
    {
        public int StructValue;

        public bool Equals(CustomValueTypeEquitable other)
        {
            return StructValue == other.StructValue;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is CustomValueTypeEquitable && Equals((CustomValueTypeEquitable) obj);
        }

        public override int GetHashCode()
        {
            return StructValue;
        }
    }
    
    public class CustomIntComparer : IEqualityComparer<int>
    {
        public bool Equals(int x, int y)
        {
            return x == y;
        }

        public int GetHashCode(int obj)
        {
            return obj;
        }
    }

    public class CustomValueTypeComparer : IEqualityComparer<CustomValueType>
    {
        public bool Equals(CustomValueType x, CustomValueType y)
        {
            return x.StructValue == y.StructValue;
        }

        public int GetHashCode(CustomValueType obj)
        {
            return obj.StructValue;
        }
    }
    
    //public class SameHashCodeComparer : IEqualityComparer<CustomValueType>
}